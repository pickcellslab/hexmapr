# README

This repository contains R scripts to generate hexagonal bin plots which are useful to visualise how a given marker of interest is preferentially localised in colonies of stem cells grown on micropatterns[^1].

## Usage

  * Clone or download and unzip this repository to a directory of your choice.
  * Open the binnedmap_template.R script in [RStudio](https://www.rstudio.com/)
  * Follow the instructions documented in the binnedmap_template.R file

An example dataset is provided with this repository (in the /data directory). 


## Reference 

[^1]: [Geometrical confinement controls the asymmetric patterning of Brachyury in cultures of pluripotent cells](http://dev.biologists.org/content/145/18/e1801). Blin G., Wisniewski D, Picart C, Thery M, Puceat M, Lowell S. Development. 2018 Aug 16. pii: dev.166025. doi: 10.1242/dev.166025. [Epub ahead of print] PMID: 30115626 
