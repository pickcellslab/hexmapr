#' Generates a data frame with a list of coordinates which can be used to draw the outline of the micropatterns in binned plots
#'
#' @param center The xy coordinates for the center of the outline (default is c(0,0))
#' @param d1 diameter of the ellipse in the x axis (default is 1)
#' @param d2 diameter of the ellipse in the y axis (default is 1)
#' @param npoints Number of points in the outline (default is 100)
#' 
#' @return A dataframe containing the list of coordinates
#'
elFun <-
function(center = c(0,0), d1=1, d2=1, npoints = 100){
    r1 = d1 / 2
    r2 = d2 / 2
    tt <- seq(0,2*pi,length.out = npoints)
    xx <- center[1] + r1 * cos(tt)
    yy <- center[2] + r2 * sin(tt)
    return(data.frame(x = xx, y = yy))
}
